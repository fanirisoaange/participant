<?php

namespace App\Repository;

use App\Entity\Participant;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\ORM\Query;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method Participant|null find($id, $lockMode = null, $lockVersion = null)
 * @method Participant|null findOneBy(array $criteria, array $orderBy = null)
 * @method Participant[]    findAll()
 * @method Participant[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class ParticipantRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Participant::class);
    }

    public function findAllQuery(): Query
    {
        return $this->createQueryBuilder('p')
            ->getQuery()
        ;
    }

    public function deleteMultiple($ids): Query
    {
        return $query =  $this->createQueryBuilder('p')
            ->delete(Participant::class, 'p')
            ->where('p.id in (:ids)')
            ->setParameter(':ids', $ids)
            ->getQuery();
    }

    public function findByParamsQuery($lastname, $society): Query
    {
        $qb = $this->createQueryBuilder('p');
        $qb
            ->select('p', 's')
            ->join('p.society', 's')
        ;
        if ($lastname) {
            $qb->andWhere( $qb->expr()->like('lower(p.lastname)', 'lower(:lastname)'))
                ->setParameter('lastname', '%'.$lastname.'%');
        }
        if ($society) {
            $qb->andWhere( $qb->expr()->like('lower(s.name)', 'lower(:societyName)'))
                ->setParameter('societyName', '%'.$society.'%');
        }

        return $qb->getQuery();
    }

    public  function getParticipantByActivationKey($activationKey){
        $qb = $this->createQueryBuilder('p');
        $qb
            ->select('s')
            ->andWhere('MD5(s.id) =:activationKey' )
            ->setParameter('activationKey' , $activationKey)
        ;

        return $qb->getQuery()->getOneOrNullResult();
    }
}
