<?php

namespace App\Repository;

use App\Entity\Society;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\ORM\Query;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method Society|null find($id, $lockMode = null, $lockVersion = null)
 * @method Society|null findOneBy(array $criteria, array $orderBy = null)
 * @method Society[]    findAll()
 * @method Society[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class SocietyRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Society::class);
    }


    public function findAllQuery(): Query
    {
        return $this->createQueryBuilder('s')
            ->getQuery()
            ;
    }

    public function findByParamsQuery($name): Query
    {
        $qb = $this->createQueryBuilder('s');
        $qb
            ->select('s')
        ;
        if ($name) {
            $qb->andWhere( $qb->expr()->like('lower(s.name)', 'lower(:name)'))
                ->setParameter('name', '%'.$name.'%');
        }
        return $qb->getQuery();
    }
}
