<?php


namespace App\Controller;


use App\Entity\Participant;
use Twig\Environment;

class Notification
{
    /**
     * @var \Swift_Mailer
     */
    private $mailer;

    /**
     * @var Environment
     */
    private $renderer;

    public function __construct(\Swift_Mailer $mailer, Environment $renderer)
    {
        $this->mailer = $mailer;
        $this->renderer = $renderer;
    }

    public function sendMailActivation($activationKey, Participant $participant){
        $message = (new \Swift_Message('Compte activation'))
            ->setFrom('angefaniry@gmail.com')
            ->setTo($participant->getEmail())
            ->setBody(
                $this->renderer->render(
                    'email/activation.html.twig',
                    [
                        'name' => $participant->getLastname(),
                        'activationKey' => $activationKey
                        ]
                ),
                'text/html'
            )
        ;
        $this->mailer->send($message);
    }

}