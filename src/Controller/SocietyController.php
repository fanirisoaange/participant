<?php 
namespace App\Controller;

use App\Entity\Society;
use App\Form\SocietyType;
use App\Repository\SocietyRepository;
use Doctrine\ORM\EntityManagerInterface;
use Knp\Component\Pager\PaginatorInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class SocietyController  extends  AbstractController{

    /**
     * @param SocietyRepository $repository
     * @param EntityManagerInterface $em
     */
    private $twig;

    public function __construct(SocietyRepository $repository, EntityManagerInterface $em){
        $this->repository = $repository;
        $this->em =$em;
    }

    /**
     * @Route("/society", name="society.index")
     * @param PaginatorInterface $paginator
     * @param Request $request
     * @return Response
     */
    public function index(PaginatorInterface $paginator, Request $request) : Response {
        $name = $request->get('name');
        $societies = $paginator->paginate(
            $this->repository->findByParamsQuery($name),
            $request->query->getInt('page',1),
            10);

        return $this->render('society/index.html.twig', [
                'societies' => $societies,
                'filter' => [
                    'name' => $name,
                ]
            ]);
    }

    /**
     * @Route("/society/create", name="society.new")
     * @param Request $request
     * @return Response
     */
    public function create( Request $request) : Response {
        $society = new Society();
        $form  = $this->createForm(SocietyType::class, $society);
        $form->handleRequest($request);
        if($form->isSubmitted() && $form->isValid()){
            $this->em->persist($society);
            $this->em->flush();
            return $this->redirectToRoute('society.index');
        }
        return $this->render('society/detail.html.twig', [
            'society' =>$society,
            'form' => $form->createView(),
            'title' => 'Création d\'une société'
        ]);
    }

    /**
     * @Route("/society/{id}", name="society.edit", methods="GET|POST")
     * @param Society $society
     * @param Request $request
     * @return Response
     */
    public function edit(Society $society, Request $request) : Response {
        $form =$this->createForm(SocietyType::class, $society);
        $form->handleRequest($request);
        if($form->isSubmitted() && $form->isValid()){
            $this->em->flush();
            return $this->redirectToRoute('society.index');
        }
        return $this->render('society/detail.html.twig', [
            'society' =>$society,
            'form' => $form->createView(),
            'title' => 'Modification d\'une société'
        ]);
    }

    /**
     * @Route("/society/{id}", name="society.delete", methods="DELETE")
     * @param Society $society
     * @param Request $request
     * @return Response
     */
    public function delete(Society $society, Request $request) : Response {
        if($this->isCsrfTokenValid('delete'.$society->getId(), $request->get('_token'))){
            $this->em->remove($society);
            $this->em->flush();
        }
        return $this->redirectToRoute('society.index');
    }

}