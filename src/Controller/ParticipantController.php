<?php
namespace App\Controller;

use App\Entity\Participant;
use App\Form\ParticipantType;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\Persistence\ObjectManager;
use Knp\Component\Pager\PaginatorInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use App\Repository\ParticipantRepository;

class ParticipantController extends  AbstractController {

    /**
     * ParticipantController constructor.
     * @param ParticipantRepository $repository
     * @param EntityManagerInterface $em
     */
    public function __construct(ParticipantRepository $repository, EntityManagerInterface $em)
    {
        $this->repository = $repository;
        $this->em =$em;
    }

    /**
     * @Route("/participant", name="participant.index")
     * @param PaginatorInterface $paginator
     * @param Request $request
     * @return Response
     */
    public function index(PaginatorInterface $paginator, Request $request) : Response {
        $lastname = $request->get('lastname');
        $society = $request->get('society');
        $participants = $paginator->paginate(
            $this->repository->findByParamsQuery($lastname, $society),
            $request->query->getInt('page',1),
            10);
        return $this->render('participant/index.html.twig', [
            'participants' => $participants,
            'filter' => [
                'lastname' => $lastname,
                'society' => $society
            ]
        ]);
    }

    /**
     * @Route("/participant/create", name="participant.new")
     * @param Request $request
     * @param Notification $notification
     * @return Response
     */
    public function create( Request $request, Notification $notification) : Response {
        $participant= new Participant();
        $form  =$this->createForm(ParticipantType::class, $participant);
        $form->handleRequest($request);
        if($form->isSubmitted() && $form->isValid()){
            $this->em->persist($participant);
            $this->em->flush();
            $activationKey = md5($participant->getId().$participant->getEmail()."activation");
            $participant->setActivationKey($activationKey);
            $this->em->flush();
            $notification->sendMailActivation($activationKey, $participant);
            $this->addFlash('success', 'Enregistrement avec succes');
            return $this->redirectToRoute('participant.index');
        }
        return $this->render('participant/detail.html.twig', [
            'participant' =>$participant,
            'form' => $form->createView(),
            'title' => 'Création d\'un participant'
        ]);
    }

    /**
     * @Route("/participant/{id}", name="participant.edit", methods="GET|POST")
     * @param Participant $participant
     * @param Request $request
     * @return Response
     */
    public function edit(Participant $participant, Request $request) : Response {
        $form =$this->createForm(ParticipantType::class, $participant);
        $form->handleRequest($request);
        if($form->isSubmitted() && $form->isValid()){
            $this->em->flush();
            return $this->redirectToRoute('participant.index');
        }
        return $this->render('participant/detail.html.twig', [
            'participant' =>$participant,
            'form' => $form->createView(),
            'title' => 'Modification d\'un participant'
            ]);
    }

    /**
     * @Route("/participant/{id}", name="participant.delete", methods="DELETE")
     * @param Participant $participant
     * @param Request $request
     * @return Response
     */
    public function delete(Participant $participant, Request $request) : Response {
        if($this->isCsrfTokenValid('delete'.$participant->getId(), $request->get('_token'))){
            $this->em->remove($participant);
            $this->em->flush();
        }
        return $this->redirectToRoute('participant.index');
    }

    /**
     * @Route("/participant_deletemultiple", name="participant.delete.multiple")
     * @param Request $request
     * @return Response
     */
    public function deleteMultiple(Request $request) : Response {
        $ids = $request->get('_ids') ? explode ( ',' , $request->get('_ids')) : [];
        dump($ids);
        if(sizeof($ids) > 0 ) {
            $query = $this->repository->deleteMultiple($ids);
            $query->execute();
        }
        return $this->redirectToRoute('participant.index');
    }


    /**
     * @Route("/activate/{activationKey}", name="activate.account")
     * @return Response
     */
    public function activateAccount( $activationKey) : Response {
        $participant =  $this->repository->findOneBy([ 'activationKey' => $activationKey , 'active' => 0]);
        if($participant){
            $participant->setActive(1);
            $this->em->persist($participant);
            $this->em->flush();
            $message = $participant->getLastname().", votre compte a été bien activé";
        } else {
            $message = "Erreur";
        }

        return $this->render('participant/participant.html.twig', [
            'message' =>$message,
        ]);
    }

    /**
     * @Route("/participant_search", name="participant.search")
     * @param PaginatorInterface $paginator
     * @param Request $request
     * @return Response
     */
    public function search(PaginatorInterface $paginator, Request $request) : Response {
        dump($request->get('lastname'));
        $lastname = $request->get('lastname');
        $society = $request->get('society');
        $participants = $paginator->paginate(
            $this->repository->findByParams($lastname, $society),
            $request->query->getInt('page',1),
            10);
        dump($participants);
        return $this->render('participant/index.html.twig', compact('participants'));
    }
}