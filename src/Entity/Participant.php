<?php

namespace App\Entity;

use App\Repository\ParticipantRepository;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=ParticipantRepository::class)
 */
class Participant
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $lastname;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $firstname;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $email;

    /**
     * @ORM\Column(type="boolean", options={"default" : 0})
     */
    private $active = 0;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $activationKey = "";

    /**
     * @ORM\ManyToMany(targetEntity="App\Entity\Society", inversedBy="participant")
     * @ORM\JoinTable(name="society_participant",
     *      joinColumns={@ORM\JoinColumn(name="participant_id", referencedColumnName="id", onDelete="SET NULL")},
     *      inverseJoinColumns={@ORM\JoinColumn(name="society_id", referencedColumnName="id")}
     * )
     */
    private $society;

    public function __construct()
    {
        $this->society = new \Doctrine\Common\Collections\ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getLastname(): ?string
    {
        return $this->lastname;
    }

    public function setLastname(string $lastname): self
    {
        $this->lastname = $lastname;

        return $this;
    }

    public function getFirstname(): ?string
    {
        return $this->firstname;
    }

    public function setFirstname(?string $firstname): self
    {
        $this->firstname = $firstname;

        return $this;
    }

    public function getEmail(): ?string
    {
        return $this->email;
    }

    public function setEmail(string $email): self
    {
        $this->email = $email;

        return $this;
    }

    public function getActive(): ?bool
    {
        return $this->active;
    }

    public function setActive(bool $active): self
    {
        $this->active = $active;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getSociety()
    {
        return $this->society;
    }

    /**
     * @param mixed $society
     */
    public function setSociety($society): void
    {
        $this->society = $society;
    }

    public function __toString()
    {
        return $this->lastname;
    }

    /**
     * @return mixed
     */
    public function getActivationKey()
    {
        return $this->activationKey;
    }

    /**
     * @param mixed $activationKey
     */
    public function setActivationKey($activationKey): void
    {
        $this->activationKey = $activationKey;
    }




}
