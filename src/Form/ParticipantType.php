<?php

namespace App\Form;

use App\Entity\Participant;
use App\Entity\Society;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class ParticipantType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('lastname', null, [
                'label' => 'Nom',
                'required' => true,
            ])
            ->add('firstname', null, [
                'label' => 'Prénom'
            ])
            ->add('email', EmailType::class, [
                'required' => true,
            ])
            ->add('society', EntityType::class, [
                'class' => Society::class,
                'multiple' => true,
                'label' => 'Société',
                'required' => true,
//                'group_by' => 'type',
//                'attr' => [
//                    'class' => 'deviceMultiSelect',
//                ]
            ]);
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Participant::class,
        ]);
    }
}
